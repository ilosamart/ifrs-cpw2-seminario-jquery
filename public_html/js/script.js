function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function start_dance(who){
  if(!who.dancing) {
      who.dancing=true;
      who.timer = setInterval(function(){start_dance(who);},120);
  }
  var tmpStr;
  if (!who.style || !who.style.backgroundPosition) {
    tmpStr = window.getComputedStyle(
	document.querySelector('#'+who.id)
        ).getPropertyValue('background-position');
  }
  else {
    tmpStr = who.style.backgroundPosition;   
  }
  tmpStr = replaceAll("px","",tmpStr);
  tmpStr = replaceAll(";","",tmpStr);
  x = tmpStr.split(" ")[0];
  y = tmpStr.split(" ")[1];
  who.style.backgroundPosition = ((x-20)%540)+"px "+y+"px";  
}
 
function stop_dance(who){
  who.dancing=false;
  clearTimeout(who.timer);
}

function manage_dance(who) {
    if(who.dancing) {
        stop_dance(who);
    }
    else {
        start_dance(who);
    }
}


/*
 * Drag and drop functions
 */

function drag_start_dance( event, ui) {
    start_dance(ui.helper.context);
}

function drag_stop_dance( event, ui) {
    stop_dance(ui.helper.context);
}

function avisa_drop( event, ui ) {
  var draggable = ui.draggable;
  if(draggable) {
      drag_stop_dance(event, ui);
      drag_start_dance(event, ui);
  }
  alert( 'O boneco com ID "' + draggable.attr('id') + '" foi largado em mim!' );
}